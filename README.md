Welcome to Tori's Turing Machine Assembler 5000! 
This excellent program is easy to run (but not easy to create).
Instructions: 
1. Download the latest version of Visual Studio if it is not already on your computer. 
2. Upload and run the 'theoryprojectno2' file in this repository. 
Thank you!
Tori Lineberry